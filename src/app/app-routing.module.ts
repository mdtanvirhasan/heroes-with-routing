import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroDetailsComponent } from './hero-details/hero-details.component';
import { HeroComponent } from './hero/hero.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {path:'',redirectTo:'hero',pathMatch:'full'},
  {path:'hero', component:HeroComponent},
  {path:'hero-details/:id',component:HeroDetailsComponent},
  {path:'hero-details/edit/:id',component:EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
