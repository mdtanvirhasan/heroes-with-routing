import { Component, OnInit } from '@angular/core';
import {HEROES} from '../hero_description';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  heroes=HEROES.sort((a:any,b:any)=>b.power-a.power);
  constructor() { }

  ngOnInit(): void {
  }

}
