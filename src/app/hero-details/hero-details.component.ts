import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HEROES} from '../hero_description';


@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.css']
})
export class HeroDetailsComponent implements OnInit {
  heroes=HEROES;
  id:any;
  
  
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);


  }

}
