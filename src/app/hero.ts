export interface hero{
    id:number;
    name:string;
    power:number;
    description:string;
    date:Date;
}